package store;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Music;

public class MusicStoreLogic implements MusicStore {

	ConnectionFactory factory ;
	MusicStoreLogic() {
		factory = ConnectionFactory.getInstance();
	}
	@Override
	public Music read(int id) {
		Connection conn = null;
		ResultSet rs = null;
		Music music = null;
		PreparedStatement pstmt = null;
		String sql = "SELECT NAME,ARTIST_NAME,ALBUM_TITLE,IMAGE,AGENT_NAME FROM MUSIC_TB WHERE ID = ?";
		
		try {
			conn = factory.createConnection();
			
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,id);
			
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				music = new Music();
				
				music.setId(id);
				music.setName(rs.getString("NAME"));
				music.setArtist(rs.getString("ARTIST_NAME"));
				music.setAlbum(rs.getString("ALBUM_TITLE"));
				music.setImage(rs.getString("IMAGE"));
				music.setAgent(rs.getString("AGENT_NAME"));
				

				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if(conn!=null)conn.close();
				if(rs!=null)rs.close();
				if(pstmt!=null)pstmt.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return music;

	}

	@Override
	public List<Music> readByName(String name) {
		
		List<Music> list = new ArrayList<>();
		Connection conn = null;
		ResultSet rs = null;
		Music music = null;
		PreparedStatement pstmt = null;
		String sql = "SELECT ID,ARTIST_NAME,ALBUM_TITLE,IMAGE,AGENT_NAME FROM MUSIC_TB WHERE NAME = ?";
		
		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, name);
			
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				music = new Music();
				music.setId(rs.getInt("ID"));
				music.setName(name);
				music.setArtist(rs.getString("ARTIST_NAME"));
				music.setAlbum(rs.getString("ALBUM_TITLE"));
				music.setImage(rs.getString("IMAGE"));
				music.setAgent(rs.getString("AGENT_NAME"));
				
				list.add(music);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			
			try {
				
				if(conn!=null)conn.close();
				if(pstmt!=null)pstmt.close();
				if(rs!=null)rs.close();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	@Override
	public List<Music> readAll() {
		List<Music> list = new ArrayList<>();
		Connection conn = null;
		ResultSet rs = null;
		Music music = null;
		PreparedStatement pstmt = null;
		String sql = "SELECT ID,ARTIST_NAME,ALBUM_TITLE,IMAGE,AGENT_NAME FROM MUSIC_TB";
		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);

			
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				music = new Music();
				music.setId(rs.getInt("ID"));
				music.setName(rs.getString("NAME"));
				music.setArtist(rs.getString("ARTIST_NAME"));
				music.setAlbum(rs.getString("ALBUM_TITLE"));
				music.setImage(rs.getString("IMAGE"));
				music.setAgent(rs.getString("AGENT_NAME"));
				
				list.add(music);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			
			try {
				
				if(conn!=null)conn.close();
				if(pstmt!=null)pstmt.close();
				if(rs!=null)rs.close();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	

}
