package store;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Music;

public class UserMusicStoreLogic implements UserMusicStore {
	
	ConnectionFactory factory;
	
	public UserMusicStoreLogic() {
	
		factory = ConnectionFactory.getInstance();
	}

	@Override
	public boolean create(String userId, int musicId) {
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "INSERT INTO USER_MUSIC_TB(MUSIC_ID,USER_ID) VALUES (?,?)";
		
		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1,musicId);
			pstmt.setString(2, userId);;
			
			int r = pstmt.executeUpdate();
			
			if(r==1)return true;
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
				try {
					if(conn!=null)conn.close();
					if(pstmt!=null)pstmt.close();
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return false;
	}

	@Override
	public boolean delete(String userId, int musicId) {
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "DELETE FROM USER_MUSIC_TB WHERE MUSIC_ID = ?, USER_ID = ?";
		
		
		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1,musicId);
			pstmt.setString(2, userId);
			
			int r = pstmt.executeUpdate();
			
			if(r==1)return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
				try {
					if(conn!=null)conn.close();
					if(pstmt!=null)pstmt.close();
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		}
		return false;
	}

	@Override
	public boolean existUserMusic(String userId, int musicId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Music> readMusicsByUser(String userId) {
			
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "SELECT id,name,artist_name,album_title,image,agent_name FROM MUSIC_TB WHERE id = ("
				+ "SELECT MUSIC_ID FROM USER_MUSIC_TB WHERE USER_ID = ?";
		List<Music> list = new ArrayList();
		Music music = null;
		
		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userId);
			
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				music = new Music();
				
				music.setId(rs.getInt("id"));
				music.setName(rs.getString("name"));
				music.setArtist(rs.getString("artist_name"));
				music.setAlbum(rs.getString("album_title"));
				music.setImage(rs.getString("image"));
				music.setAgent(rs.getString("agent_name"));
				
				list.add(music);
				
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if(conn!=null)conn.close();
				if(pstmt!=null)pstmt.close();
				if(rs!=null)rs.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return list;
	}

}
